import { createStore } from 'vuex'

export default createStore({
  state: {
    list:[{id:0,name:'鱼香肉丝',money:'12',num:0},
    {id:1,name:'宫保鸡丁',money:'14',num:0},
    {id:2,name:'土豆丝',money:'10',num:0},
    {id:3,name:'米饭',money:'2',num:0}],
    newlist:[],
    allnum:0,
    allmoney:0
  },
  mutations: {
    addbtn(state,value){
      state.newlist=[...state.newlist,value]
    },
    addmoney(state,value){
      state.list.map(items=>{
        if(items==value){
          items.num=Number(items.num)+1
        }
      })
    },
    clebtn(state,value){
      state.list.map(item=>{
        if(item.name==value.name){
          item.num=Number(item.num)-1
        }
      })
      if(!value.num){
        state.newlist=state.newlist.filter(item=>{
          return item.name!=value.name
        })
      }
    },
    allclebtn(state){
      state.list.map(item=>{
        item.num=0
      })
      state.newlist=[]
    },
    cleitm(state,value){
      state.list[value].num=0
      state.newlist= state.newlist.filter(item=>{
        return item.name!=state.list[value].name
      })
    },
    setlist(state){
      state.list=JSON.parse(localStorage.getItem('list'))||[]
    }
  },
  actions: {
    addbtn({commit},item){
      if(!item.num){
        commit('addbtn',item)
      }
    }
  },
  modules: {
  }
})
