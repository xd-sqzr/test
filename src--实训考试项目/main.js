import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
// import store from './store'
import 'jquery/dist/jquery.slim'
import 'bootstrap/dist/css/bootstrap.css'
// 移动端配置
import './rem'
import Loading from './view/Loading.vue'
const app=createApp(App)
app.use(router).mount('#app')
app.component('Loading',Loading)
