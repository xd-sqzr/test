import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
// import store from './store'
import 'jquery/dist/jquery.slim'
import 'bootstrap/dist/css/bootstrap.css'
import './rem'
createApp(App).use(router).mount('#app')
